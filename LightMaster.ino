#include <DS3231.h>
#include <BH1750FVI.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

DS3231  rtc(SDA, SCL);
BH1750FVI LightSensor(BH1750FVI::k_DevModeContLowRes);
LiquidCrystal_I2C lcd(0x27,16,2);

// for LCD backlight
boolean lastLcdBacklightButton = LOW;
boolean currentLcdBacklightButton = LOW;
boolean isLcdBacklightOn = false;

// light
int SUNNY_WEATHER_LUX = 2500; // is turned ON when value on the light sensor is smaller than <SUNNY_WEATHER_LUX>
int START_LIGHT_HOUR = 8; // inclusive
int END_LIGHT_HOUR = 19; // exclusive
int RELAY_PIN_LIGHT = 2;

// gas
float COLD_TEMP = 21;
int START_GAS_FIRE_MORNING_HOUR = 6; // morning gas fire - inclusive hour
int END_GAS_FIRE_MORNING_HOUR = 8; // morning gas fire - exclusive hour
int START_GAS_FIRE_EVENING_HOUR = 18; // evening gas fire - inclusive hour
int END_GAS_FIRE_EVENING_HOUR = 22; // evening gas fire - exclusive hour
int RELAY_PIN_GAS = 4;

int LCD_BACKLIGHT_PIN = 12;
int MIN_INTERVAL_BEETWEEN_TOOGLE_LIGHT_SEC = 20;

// the last toogle of light
long lastToogleSec = 0;

void setup() {
  // Setup Serial connection
  Serial.begin(115200);
  // Initialize the rtc object
  rtc.begin();
  // init light sensor
  LightSensor.begin();

  lastToogleSec = getCurrentTimeSec() - MIN_INTERVAL_BEETWEEN_TOOGLE_LIGHT_SEC;

  pinMode(RELAY_PIN_LIGHT, OUTPUT);
  pinMode(RELAY_PIN_GAS, OUTPUT);
  pinMode(LCD_BACKLIGHT_PIN, INPUT);

  // setup display
  lcd.init();                     
  // lcd.backlight();// Включаем подсветку дисплея
  
  // The following lines can be uncommented to set the date and time
  //rtc.setDOW(SATURDAY);     // Set Day-of-Week to SUNDAY
  //rtc.setTime(17, 31, 0);     // Set the time to 12:00:00 (24hr format)
  //rtc.setDate(13, 7, 2019);   // Set the date to January 1st, 2014
}

void loop() {
  manageLcdBacklight();
  
  if (isIntervalBeetweenTooglesMatch()) {
    toogleLight(isNeedToTurnOnLight());
    toogleFire(isNeedToLightAFire());
    lastToogleSec = getCurrentTimeSec();
  }
  
  displayDataOnLcd(LightSensor.GetLightIntensity(), rtc.getTimeStr(), rtc.getTemp());
  
  // Wait one second before repeating (5 in the debounce() method + 995)
  delay (995);
}

/*
 *return true if >= than <MIN_INTERVAL_BEETWEEN_TOOGLE_LIGHT_SEC> seconds passed after the last toogle 
*/
boolean isIntervalBeetweenTooglesMatch() {
  return getCurrentTimeSec() - lastToogleSec >= MIN_INTERVAL_BEETWEEN_TOOGLE_LIGHT_SEC;
}

boolean isNeedToTurnOnLight() {
  // turn on the light if time is from <START_LIGHT_HOUR> to <END_LIGHT_HOUR> (exclusive)
  int currentHour = getCurrentHour();
  uint16_t currentLuxValue = LightSensor.GetLightIntensity();

  return currentHour >= START_LIGHT_HOUR && currentHour < END_LIGHT_HOUR && currentLuxValue < SUNNY_WEATHER_LUX;
}

boolean isNeedToLightAFire() {
  int currentHour = getCurrentHour();
  float currentTemp = rtc.getTemp();

  // always light a gas fire when lcd backlight is on
  // or now is morning time from <START_GAS_FIRE_MORNING_HOUR> to <END_GAS_FIRE_MORNING_HOUR> and currentTemp < COLD_TEMP
  // or now is evening time from <START_GAS_FIRE_EVENING_HOUR> to <END_GAS_FIRE_EVENING_HOUR> and currentTemp < COLD_TEMP
  return isLcdBacklightOn || 
         (currentHour >= START_GAS_FIRE_MORNING_HOUR && currentHour < END_GAS_FIRE_MORNING_HOUR && currentTemp < COLD_TEMP) ||
         (currentHour >= START_GAS_FIRE_EVENING_HOUR && currentHour < END_GAS_FIRE_EVENING_HOUR && currentTemp < COLD_TEMP);
}

void toogleLight(boolean on) {
  if (on) {
    Serial.println("Light is on");
    digitalWrite(RELAY_PIN_LIGHT, LOW);
  } else {
    Serial.println("Light is off");
    digitalWrite(RELAY_PIN_LIGHT, HIGH);
  }
}

void toogleFire(boolean on) {
  if (on) {
    Serial.println("Fire is on");
    digitalWrite(RELAY_PIN_GAS, LOW);
  } else {
    Serial.println("Fire is off");
    digitalWrite(RELAY_PIN_GAS, HIGH);
  }
}

void displayDataOnLcd(int lux, String currentTime, float temperature) {
  // display current time on the first line
  lcd.setCursor(0, 0);
  lcd.print(currentTime + "  t" + String(temperature));
  
  // display lux value on the second line
  // clear previous lux value
  lcd.setCursor(0, 1);
  lcd.print("             ");
  // display current lux value
  lcd.setCursor(0, 1);
  lcd.print("Light: " + String(lux));
}

//////////////// TIME ////////////////////

int getCurrentHour() {
  String currentTimeStr = rtc.getTimeStr();
  if (currentTimeStr.length() > 0) {
    int separatorIndex = currentTimeStr.indexOf(":");
    if (separatorIndex != -1) {
      return currentTimeStr.substring(0, separatorIndex).toInt();
    }
  }
}

long getCurrentTimeSec() {
  return rtc.getUnixTime(rtc.getTime());
}

////////////// LCD backlight /////////////
void manageLcdBacklight() {
  currentLcdBacklightButton = debounce(lastLcdBacklightButton);
  if (lastLcdBacklightButton == LOW && currentLcdBacklightButton == HIGH) {
    isLcdBacklightOn = !isLcdBacklightOn;
  }
  lastLcdBacklightButton = currentLcdBacklightButton;
  lcd.setBacklight(isLcdBacklightOn);
}

boolean debounce(boolean lastButton) {
  boolean currentButton = digitalRead(LCD_BACKLIGHT_PIN);
  if (lastButton != currentButton) {
    delay(5);
    currentButton = digitalRead(LCD_BACKLIGHT_PIN);
  }
  return currentButton;
}
